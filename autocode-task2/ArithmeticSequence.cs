﻿namespace PadawansTask2
{
    public static class ArithmeticSequence
    {
        public static int Calculate(int number, int add, int count)
        {
            var sumValue = 0;

            for (var i = 1; i <= count; ++i)
            {
                var currentMultiplier = i - 1;
                sumValue += number + add * currentMultiplier;
            }

            return sumValue;
        }
    }
}